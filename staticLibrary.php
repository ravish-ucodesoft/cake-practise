<?php
class StaticLibrary{

	const DEBUG = 2;
	const ENV = 'development'; # testing | production
	//static constants are set here 
    public static function myConstants(&$Vars){ 
        $Vars['database']        = array(
										'host'		=> 'localhost',
										'login'		=> 'root',
										'password'	=> 'FHQ35tgRfYP3',
										'database'	=> 'cakephp_practise'
								);
    } 

    //usage: StaticLibrary::vars('someVar') 

    public static function vars($v) 
    {    
   		static $Vars; 
     	if (!is_array($Vars)) { $Vars = array(); StaticLibrary::myConstants($Vars); } //setup access to the static contants in our Final library 
     	return($Vars[$v]); //return some var 
  	}
}